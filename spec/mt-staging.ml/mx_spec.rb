describe 'mt-staging.ml' do
  context 'IN MX' do
    records = [
      {
        name: 'test.mt-staging.ml',
        preference: '1',
        value: 'mx.mt-staging.ml'
      }
    ]

    records.each do |record|
      describe record[:name] do
        it { is_expected.to have_dns.with_type('MX').and_preference(record[:preference]).and_exchange(record[:value]) }
      end
    end
  end
end
