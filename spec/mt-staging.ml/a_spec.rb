describe 'mt-staging.ml' do
  context 'IN A' do
    records = [
      {
        name: 'test.mt-staging.ml',
        value: '127.0.0.1'
      }
    ]

    records.each do |record|
      describe record[:name] do
        it { is_expected.to have_dns.with_type('A').and_address(record[:value]) }
      end
    end
  end
end

