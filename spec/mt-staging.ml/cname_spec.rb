describe 'mt-staging.ml' do
  context 'IN CNAME' do
    records = [
      {
        name: 'mailtop._domainkey.mt-staging.ml',
        value: 'dkim.mailtop.com.br'
      }
    ]

    records.each do |record|
      describe record[:name] do
        it { is_expected.to have_dns.with_type('CNAME').and_domainname(record[:value]) }
      end
    end
  end
end
