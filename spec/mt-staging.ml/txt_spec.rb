describe 'mt-staging.ml' do
  context 'IN TXT' do
    records = [
      {
        name: '_dmarc.mt-staging.ml',
        value: 'v=DMARC1; p=none; pct=100; sp=none; aspf=r;'
      }
    ]

    records.each do |record|
      describe record[:name] do
        it { is_expected.to have_dns.with_type('TXT').and_data(record[:value]) }
      end
    end
  end
end
