# DNS Verify

Test your DNS records using GitLab CI.

## How to use

1. Fork the repo.
1. Duplicate the files inside the `spec` dir with your domain and records.
1. Add all records you want to verify. This is useful as a source of truth
   for your records, or if you are migrating your records from one provider
   to another.
1. On GitLab, go to `CI/CD` > `Schedules` and add a new schedule.
1. Provide all info to create a new pipeline schedule. Just remember: 
   if you are using a private CI Runner, make sure that the server is
   online at the time you scheduled the pipeline.
1. Make sure that the project has the `Failed Pipeline` alert enabled. 
   This way you will be notified by e-mail when the DNS records are set
   up differently from what is registered in the repo.